# Summary

This project has been made to handle, convert (and in future versions generate) .TID files which are coming from Megadimension Neptunia VII (and probably some other game from Idea Factory).
The current .TID versions supported are :
- 0x80
- 0x81
- 0x82
- 0x88
- 0x89
- 0x90
- 0x91
- 0x92
- 0x93
- 0x98
- 0x99
- 0x9A

# Version

This project's current version is 1.0.0.

# Dependencies

This project requires the use of :
- Be.IO 1.0.0
- ManagedSquish 2.0.0.0

# License

This project is licensed under the terms of the Simple Non Code License 2.0.2.
You can find more details about this license in the License.txt file.