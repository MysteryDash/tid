﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.2.
// The full license text can be found in the file named License.txt.
// Written originally by Alexandre Quoniou in 2016.
//

using System;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using MysteryDash.FileFormats;

namespace MysteryDash.TidConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please drag and drop a file over this executable.");
            }
            else
            {
                Console.WriteLine("Tid Converter - By MysteryDash");

                Parallel.ForEach(args, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, arg =>
                {
                    try
                    {
                        Console.WriteLine($"Processing {arg}...");
                        var tid = Tid.FromBytes(File.ReadAllBytes(arg));
                        tid.Bitmap.Save(Path.ChangeExtension(arg, "png"), ImageFormat.Png);
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine($"{arg} couldn't be opened. Details : {ex.Message}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"{arg} couldn't be read. Details : {ex.Message}");
                    }
                });

                Console.WriteLine("Conversion done !");
                Console.ReadKey();
            }
        }
    }
}
