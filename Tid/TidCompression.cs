// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.2.
// The full license text can be found in the file named License.txt.
// Written originally by Alexandre Quoniou in 2016.
//

using ManagedSquish;

namespace MysteryDash.FileFormats
{
    public enum TidCompression
    {
        None = 0,
        Dxt1 = 827611204,
        Dxt5 = 894720068
    }

    public enum TidType
    {
        UncompressedOnly,
        CompressedOnly,
        Both
    }

    public static class TidCompressionExtensions
    {
        public static SquishFlags ToSquishFlags(this TidCompression compression)
        {
            if (compression == TidCompression.Dxt1)
                return SquishFlags.Dxt1;
            if (compression == TidCompression.Dxt5)
                return SquishFlags.Dxt5;
            return 0;
        }
    }
}