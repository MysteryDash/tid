﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.2.
// The full license text can be found in the file named License.txt.
// Written originally by Alexandre Quoniou in 2016.
//

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Be.IO;
using ManagedSquish;

namespace MysteryDash.FileFormats
{
    /// <summary>
    /// This class provides methods to handle .TID files.
    /// </summary>
    public class Tid
    {
        public static readonly Dictionary<int, TidType> Versions = new Dictionary<int, TidType>
        {
            { 0x80, TidType.Both },
            { 0x81, TidType.CompressedOnly },
            { 0x82, TidType.UncompressedOnly },
            { 0x88, TidType.CompressedOnly },
            { 0x89, TidType.CompressedOnly },
            { 0x90, TidType.Both },
            { 0x91, TidType.CompressedOnly },
            { 0x92, TidType.UncompressedOnly },
            { 0x93, TidType.UncompressedOnly },
            { 0x98, TidType.Both },
            { 0x99, TidType.CompressedOnly },
            { 0x9A, TidType.UncompressedOnly }
        };

        public Bitmap Bitmap { get; set; }
        public TidCompression Compression { get; set; }
        public byte[] Filename { get; set; }
        public string ReadableFilename => Encoding.ASCII.GetString(Filename.TakeWhile(c => c != '\0').ToArray());
        public int Height => Bitmap.Height;
        public bool IsLittleEndian => (Version & 0x01) == 1;
        public uint UncompressedSize => 0x80 + (uint)Width * (uint)Height * 4;
        public byte Version { get; set; }
        public int Width => Bitmap.Width;

        public Tid(Bitmap bitmap, byte[] filename) : this(bitmap, filename, TidCompression.None)
        {

        }

        public Tid(Bitmap bitmap, byte[] filename, TidCompression compression) : this(bitmap, filename, compression, 0x90)
        {

        }

        public Tid(Bitmap bitmap, byte[] filename, TidCompression compression, byte version)
        {
            Contract.Requires<ArgumentNullException>(bitmap != null);
            Contract.Requires<ArgumentNullException>(filename != null);
            Contract.Requires<ArgumentOutOfRangeException>(filename.Length <= 32);
            Contract.Requires<ArgumentException>(Versions.ContainsKey(version));

            Bitmap = bitmap;
            Filename = filename;
            Compression = compression;
            Version = version;
        }
        
        public static Tid FromFile(string path)
        {
            Contract.Requires<ArgumentNullException>(!string.IsNullOrWhiteSpace(path));
            Contract.Requires<FileNotFoundException>(File.Exists(path));

            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return ReadStream(stream);
            }
        }

        public static Tid FromBytes(byte[] data)
        {
            Contract.Requires<ArgumentNullException>(data != null);

            using (var stream = new MemoryStream(data))
            {
                return ReadStream(stream);
            }
        }

        public static Tid ReadStream(Stream stream)
        {
            Contract.Requires<ArgumentNullException>(stream != null);
            Contract.Requires(stream.CanRead);
            Contract.Requires(stream.CanSeek);
            Contract.Requires(stream.Length >= 0x80); // Speeds up the process of checking for an End of Stream during the read of the header.
            Contract.Ensures(Contract.Result<Tid>() != null);

            if (stream.ReadByte() != 0x54 || stream.ReadByte() != 0x49 || stream.ReadByte() != 0x44)
                throw new InvalidDataException("File isn't a TID file");

            var version = (byte)stream.ReadByte();
            if (!Versions.ContainsKey(version))
                throw new InvalidVersionException($"{version} isn't a valid version that can be read.");

            using (var reader = (version & 0x01) == 1 ? new BeBinaryReader(stream, new UTF8Encoding(), true) : new BinaryReader(stream, new UTF8Encoding(), true))
            {
                stream.Seek(0x20, SeekOrigin.Begin);
                var filename = reader.ReadBytes(32);

                stream.Seek(0x44, SeekOrigin.Begin);
                var width = reader.ReadInt32();
                var height = reader.ReadInt32();

                stream.Seek(0x58, SeekOrigin.Begin);
                var dataLength = reader.ReadInt32();

                stream.Seek(0x64, SeekOrigin.Begin);
                int compressionValue = reader.ReadInt32();
                if (!Enum.IsDefined(typeof(TidCompression), compressionValue))
                    throw new InvalidCompressionMethod();
                TidCompression compression = (TidCompression)compressionValue;

                if (stream.Length < 0x80 + dataLength)
                    throw new EndOfStreamException();

                stream.Seek(0x80, SeekOrigin.Begin);
                byte[] data;
                if (version == 0x9A)
                {
                    data = reader.ReadBytes(width * height * 4);
                }
                else
                {
                    data = reader.ReadBytes(dataLength);
                }

                if (compression == TidCompression.None)
                {
                    if (version == 0x9A)
                    {
                        
                    }
                    else if ((version & 0x02) == 0x02)
                    {
                        data = SwapColorChannels(data, 3, 2, 1, 0);
                    }
                    else
                    {
                        data = SwapColorChannels(data, 2, 1, 0, 3);
                    }
                }
                else
                {
                    data = Squish.DecompressImage(data, width, height, compression.ToSquishFlags());
                    data = SwapColorChannels(data, 2, 1, 0, 3);
                }

                if (compression == TidCompression.Dxt1)
                {
                    data = FillAlpha(data, 0, 0xFF);
                }

                var bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                var bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(data, 0, bitmapData.Scan0, data.Length);
                bitmap.UnlockBits(bitmapData);

                return new Tid(bitmap, filename, compression, version);
            }
        }

        private static byte[] SwapColorChannels(byte[] raw, int first, int second, int third, int fourth)
        {
            Contract.Requires<ArgumentNullException>(raw != null);
            Contract.Requires<ArgumentException>(raw.Length % 4 == 0);
            Contract.Requires<ArgumentOutOfRangeException>(first >= 0 || first <= 3);
            Contract.Requires<ArgumentOutOfRangeException>(second >= 0 || second <= 3);
            Contract.Requires<ArgumentOutOfRangeException>(third >= 0 || third <= 3);
            Contract.Requires<ArgumentOutOfRangeException>(fourth >= 0 || fourth <= 3);

            var length = raw.Length;
            var output = new byte[length];
            for (int i = 0; i < length; i+=4)
            {
                output[i + 0] = raw[i + first];
                output[i + 1] = raw[i + second];
                output[i + 2] = raw[i + third];
                output[i + 3] = raw[i + fourth];
            }

            return output;
        }

        private static byte[] FillAlpha(byte[] raw, int index, byte value)
        {
            Contract.Requires<ArgumentNullException>(raw != null);
            Contract.Requires<ArgumentOutOfRangeException>(index >= 0 && index <= 3);

            for (int i = 0; i < index; i += 4)
            {
                raw[i] = value;
            }
            return raw;
        }

        public Tid ToBytes()
        {
            throw new NotImplementedException();
        }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(Bitmap != null);
            Contract.Invariant(Filename.Length <= 32);
            Contract.Invariant(Versions.ContainsKey(Version));
            Contract.Invariant(Compression == TidCompression.None ? Versions[Version] == TidType.Both || Versions[Version] == TidType.UncompressedOnly : Versions[Version] == TidType.Both || Versions[Version] == TidType.CompressedOnly);
        }
    }
}
